#!/bin/bash

# Find a disk that can be used
device=
IFS=$'\n'
for l in `lsblk -nd -I 8 --output KNAME,MOUNTPOINT | tr -s " "`; do
    dev=`echo $l | cut -d ' ' -f 1`
    mountpoint=`echo $l | cut -d ' ' -f 2`

    # Do not consider block devices that are currently mounted
    if [ -z  "$mountpoint" ]
    then
        device=/dev/$dev
        break
    fi

    echo "$dev --> $mountpoint"
done
echo "Found a suitable block device: $device"

# Get the labels of the different partitions
permanent_dev=${device}1
tmp_dev=${device}2
permanent_dev_lbl=`e2label $permanent_dev`
tmp_dev_lbl=`e2label $tmp_dev`

# Partition the drive
if [[ "$permanent_dev_lbl" != "STATIC" || "$tmp_dev_lbl" != "TMP" ]]; then
    echo "Formating the device $device"

    parted --script $device mklabel gpt
    parted --script $device mkpart primary ext4 2048s 1024
    parted --script $device mkpart primary ext4 1024 100%

    mkfs.ext4 -F -L STATIC ${permanent_dev}
fi

# Empty the TMP partition
mkfs.ext4 -F -L TMP ${tmp_dev}

# Mount the drives
mkdir -p /mnt/{permanent,tmp}
mount ${permanent_dev} /mnt/permanent
mount ${tmp_dev} /mnt/tmp
