#!/bin/bash

set -eu -o pipefail

# Copy the current version of the simple gateway
rsync -ar /app_ro/ /app/
cd /app

# HACK: Make sure there is a fake entrypoint
mkdir -p archiso/airootfs/app/
touch archiso/airootfs/app/entrypoint

# Configure the image
case ${METHOD} in
  docker)
    sed "s~{CONTAINER}~${CONTAINER}~g" templates/docker.service > archiso/airootfs/etc/systemd/system/infra.service
    ;;

  web)
    sed "s|{URL}|${URL}|g" templates/web.service > archiso/airootfs/etc/systemd/system/infra.service

    mkdir -p archiso/airootfs/app/
    touch archiso/airootfs/app/entrypoint
    ;;

  local)
    cp templates/local.service archiso/airootfs/etc/systemd/system/infra.service
    rsync -av --delete /usr_app/ archiso/airootfs/app/
    ;;

  *)
    echo "Unknown deployment method"
    exit 1
    ;;
esac

# Add the necessary packages
tr " " "\n" <<< "${EXTRA_PACKAGES}" >> archiso/packages.x86_64

# Generate an ssh key
mkdir out
mkdir archiso/airootfs/root/.ssh/
ssh-keygen -f out/gateway_key -P ""
cp out/gateway_key.pub archiso/airootfs/root/.ssh/authorized_keys
chmod 600 archiso/airootfs/root/.ssh/authorized_keys

# Generate the image
mkarchiso -v -w tmp -o out archiso/

# Use a consistent name for the image
mv out/*.iso out/gateway.iso
